import { useSelector } from "react-redux";
import { User } from "../models/user";
import { useEffect, useState } from "react";

export default function Home() {
    // при помощи useSelector можем получить текущий стейт
    const usersList = useSelector((gs:any) => gs.user.users);
    const userLogin = useSelector((gs:any) => gs.user.loggedUser);

    let [name, setName] = useState('');

    useEffect(() => {
       if (usersList !== undefined){
            let index = usersList.findIndex((x: User) => x.login === userLogin);
            if (index !== -1) {
                setName(usersList[index].firstName + ' ' + usersList[index].lastName);
            }
        }

    }, [userLogin, usersList]);

    return <>
   <span>Добро пожаловать, {name}!</span>
   <p/>
    <span>Я очень рад тебя видеть!</span>
    </>;
}