import { Close } from "@mui/icons-material";
import { Alert, Button, Card, CardContent, Collapse, IconButton, TextField } from "@mui/material";
import { useState } from "react";
import { Controller, FormProvider, useForm } from "react-hook-form"
import { useDispatch, useSelector } from "react-redux";
import { Actions } from "../store/userReducer";
import { User } from "../models/user";
import { useNavigate } from "react-router-dom";

export default function Register() {
    const methods = useForm();

    const { control, handleSubmit } = methods;

    const dispatch = useDispatch();
    const [showError, setShowError] = useState<boolean>(false);

    // при помощи useSelector можем получить текущий стейт
    const usersList = useSelector((gs:any) => gs.user.users);
    const navigate = useNavigate()
 
    const onSave = (data: any) => {
        // пытаемся найти введенного пользователя
        // не нашли - добавим его
        // нашли - скажем об этом
        if (usersList !== undefined){
            let index = usersList.findIndex((x: User) => x.login === data.login);
            if (index !== -1) {
                setShowError(true);
                return;
            }
        }
        // при помощи dispatch сигнализируем redux
        // что нужно расчитать state
        dispatch(Actions.setUser(data));
        navigate('/login');
    };


    return <FormProvider{...methods}>
        <Card style={{ paddingLeft: 48, margin: 8, marginLeft: 120, marginRight: 100}} sx={{ minWidth: 150 }}>
            <CardContent>
                <div>
                    <Controller
                        name="login"
                        control={control}
                        render={({ field: { value, onChange } }) => (
                            <TextField
                                value={value}
                                onChange={onChange}
                                id="login"
                                placeholder="Придумайте login"
                                label="Login"
                                variant="standard"
                                color="primary"
                                InputLabelProps={{ shrink: true }}
                                required
                                error={value === ''}
                             />
                        )}
                    />
                </div>
                <div>
                    <Controller
                        name="password"
                        control={control}
                        render={({ field: { value, onChange } }) => (
                            <TextField
                                value={value}
                                onChange={onChange}
                                id="password"
                                placeholder="Придумайте пароль"
                                label="Password"
                                variant="standard"
                                color="primary"
                                InputLabelProps={{ shrink: true }}
                                required
                                error={value === ''}
                             />
                        )}
                    />
                </div>
                <div>
                    <Controller
                        name="lastName"
                        control={control}
                        render={({ field: { value, onChange } }) => (
                            <TextField
                                value={value}
                                onChange={onChange}
                                id="lastName"
                                placeholder="Введите фамилию"
                                label="Фамилия"
                                variant="standard"
                                color="primary"
                                InputLabelProps={{ shrink: true }}
                           />
                        )}
                    />
                </div>
                <div>
                    <Controller
                        name="firstName"
                        control={control}
                        render={({ field: { value, onChange } }) => (
                            <TextField
                                value={value}
                                onChange={onChange}
                                id="firstName"
                                placeholder="Введите имя"
                                label="Имя"
                                variant="standard"
                                color="primary"
                                InputLabelProps={{ shrink: true }}
                            />
                        )}
                    />
                </div>
            </CardContent>
        </Card>
   
        <Button onClick={handleSubmit(onSave)}>Зарегистрировать</Button>

        <Collapse in={showError}>
            <Alert severity="error"
                action={
                    <IconButton
                    aria-label="close"
                    color="inherit"
                    size="small"
                    onClick={() => {
                        setShowError(false);
                    }}
                    >
                    <Close fontSize="inherit" />
                    </IconButton>
                }
                sx={{ mb: 2 }}
            >
            Пользователь с таким логином уже существует! Придумайте другой логин!
            </Alert>
        </Collapse>
    </FormProvider>;
}