import { Login, Close, Visibility, VisibilityOff } from "@mui/icons-material";
import { Alert, Button, Collapse, IconButton, InputAdornment, Stack, TextField } from "@mui/material";
import { SetStateAction, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Actions } from "../store/userReducer";
import { User } from "../models/user";
import { useNavigate } from "react-router-dom";

export default function DoLogin() {
    const dispatch = useDispatch();
    const [inputLogin, setInputLogin] = useState<string | null>();
    const [inputPassWord, setInputPassWord] = useState<string | null>();
    const [showError, setShowError] = useState<boolean>(false);
    const navigate = useNavigate()

    const changeLoginHandler = (event: { target: { value: SetStateAction<string | null | undefined>; }; }) => {
        setInputLogin(event.target.value);
      };
    const changePasswordHandler = (event: { target: { value: SetStateAction<string | null | undefined>; }; }) => {
        setInputPassWord(event.target.value);
    };

    const isLoginTrue = (element: User, index : string, array : User[])  => {
        return element.login === inputLogin && element.password === inputPassWord;
    };

    // при помощи useSelector можем получить текущий стейт
    const usersList = useSelector((gs:any) => gs.user.users);

    const onClick = (e: any) => {
        // находим залогинившегося пользователя и запоминаем его
        // не нашли - скажем об этом
        let foundUser : User | undefined = usersList?.find(isLoginTrue);
        if (foundUser !== undefined)
        {
            // при помощи dispatch сигнализируем redux
            // что нужно расчитать state
            dispatch(Actions.loginUser(foundUser.login));
            navigate('/home');
        }else{
            setShowError(true);
        }
    };

    const [view, setView] = useState<Boolean>(false);

    const handleShowPassword = () => {
        setView(!view);
    }


    return (
        <div style={{ margin: "30px", paddingLeft: "400px", minHeight: "500px", minWidth: "400px" }} >
            <Stack direction="column" gap={3} padding={4} paddingTop={14}>
                <TextField
                    itemID="login"
                    label="login"
                    color="primary"
                    placeholder="Введите Ваш login"
                    variant="standard"
                    sx={{ width: 300 }}
                    onChange={changeLoginHandler} />
                <TextField
                    label="password"
                    color="primary"
                    type={view ? "text" : "password"}
                    placeholder="Введите пароль"
                    variant="standard"
                    sx={{ width: 300 }}
                    InputProps={{ // <-- This is where the toggle button is added.
                        endAdornment: (
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleShowPassword}
                                    onMouseDown={handleShowPassword}
                                >
                                    {view ? <Visibility /> : <VisibilityOff />}
                                </IconButton>
                            </InputAdornment>
                        )
                    }}
                    onChange={changePasswordHandler} />
                <Button startIcon={<Login />} sx={{ width: 300 }}
                    onClick={onClick}
                >Войти</Button>
            </Stack>

            <Collapse in={showError}>
                <Alert severity="error"
                action={
                    <IconButton
                    aria-label="close"
                    color="inherit"
                    size="small"
                    onClick={() => {
                        setShowError(false);
                    }}
                    >
                    <Close fontSize="inherit" />
                    </IconButton>
                }
                sx={{ mb: 2 }}
                >
                Такой пользователь не найден! Зарегистрируйтесь!
                </Alert>
            </Collapse>
        </div >
    );
}


