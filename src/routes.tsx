import { Routes, Route } from "react-router-dom";

import Home from './pages/home';
import Notfound from './pages/notfound';
import DoLogin from './pages/login';
import Register from './pages/register';
import Init from "./pages/init";


const AppRoutes = () => {
    return (
        <Routes>
          <Route path='/' element={<Init />} />
          <Route path="login" element={<DoLogin />} /> 
          <Route path="register" element={<Register />} />
          <Route path='home' element={<Home />} />
         <Route path="*" element={<Notfound />} />
        </Routes>
    );
}

export default AppRoutes;
