import { User } from "../models/user";

export const Actions = Object.freeze({
    setUser: (user: User) => ({ type: '[USER_STATE] USER_REGISTER', payload: user }),
    loginUser: (userLogin: string) => ({ type: '[USER_STATE] USER_LOGIN', payload: userLogin }),
    logoutUser: () => ({ type: '[USER_STATE] USER_LOGOUT', payload: undefined }),
});


interface State {
    users: User[];
    loggedUser: string | undefined;
}

const initialState: State = {
    users: [],
    loggedUser: undefined
}


const userReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case'[USER_STATE] USER_REGISTER':
            const list = [...state.users];

            const item = action.payload;
            const index = list.findIndex(x => x.login === item.login);
            if (index === -1) {
                list.push(item);
            } 
            const newState = { ...state, users: [...list] };
            return newState;
        case '[USER_STATE] USER_LOGIN':
            return { ...state, loggedUser: action.payload };
        case '[USER_STATE] USER_LOGOUT':
            return { ...state, loggedUser: undefined };
        default:
            return state;
    }
};
export default userReducer;