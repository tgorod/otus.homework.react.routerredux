import React from 'react';
import {
  Link,
  BrowserRouter
} from "react-router-dom";
import './App.css';

import AppRoutes from './routes';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <nav>
          <ul>
            <li>
              <Link to={'/login'}>Войти</Link>
            </li>
            <li>
              <Link to={'/register'}>Зарегистрироваться</Link>
            </li>
            <li>
              <Link to={'/'}>Выйти</Link>
            </li>
          </ul>
        </nav>
        <AppRoutes />

      </BrowserRouter>
    </div>
  );
}

export default App;
